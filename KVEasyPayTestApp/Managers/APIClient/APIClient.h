//
//  APIClient.h
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <Foundation/Foundation.h>

#define API [APIClient sharedInstance]

#define EN_LOGS 1
#define DEBUG_SERVER 1

#if DEBUG_SERVER
    #define API_URL @"http://sandbox.easypay.ua:8084/"
#else

#endif

@interface APIClient : NSObject

+ (APIClient*)sharedInstance;


- (void)updateFirstName:(NSString*)firstName andLastName:(NSString*)lastName withFinish:(void(^)(BOOL success, NSError *error))finish_block;
- (void)loginWithUsername:(NSString*)username andPassword:(NSString*)password withFinish:(void(^)(BOOL success, NSError *error))finish_block;

-(void)getCurrentUserProfileWithFinish:(void(^)(id responseObject, NSError *error))finish_block;

- (void) POST:(NSString *)URLString parameters:(id)parameters finish:(void (^)(id responseObject, NSError *error))finish_block;
- (void) GET:(NSString *)URLString parameters:(id)parameters finish:(void (^)(id responseObject, NSError *error))finish_block;
@end
