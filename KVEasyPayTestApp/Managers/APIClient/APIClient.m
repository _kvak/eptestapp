//
//  APIClient.m
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "APIClient.h"
#import "AFNetworking.h"
#import "EPToken.h"

static APIClient *sharedInstance;

@interface APIClient ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic, strong) EPToken *currentToken;

@end

@implementation APIClient


+ (APIClient*)sharedInstance {
    if (!sharedInstance) {
        sharedInstance = [APIClient new];
    }
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _manager = [AFHTTPSessionManager manager];
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    }
    
    return self;
}

- (void)updateFirstName:(NSString*)firstName andLastName:(NSString*)lastName withFinish:(void(^)(BOOL success, NSError *error))finish_block {
    NSDictionary *params = @{@"firstName"   : firstName,
                             @"lastName"   : lastName
                             };
    
    NSString *authorization = [NSString stringWithFormat:@"%@ %@", _currentToken.tokenType, _currentToken.accessToken];
    
    [_manager.requestSerializer setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    [self POST:@"profile/update" parameters:params finish:^(id responseObject, NSError *error) {
        if (finish_block) {
            finish_block(error == nil, error);
        }
    }];

}

- (void)loginWithUsername:(NSString*)username andPassword:(NSString*)password withFinish:(void (^)(BOOL success, NSError *error))finish_block{
    NSDictionary *params = @{@"grant_type" : @"password",
                             @"username"   : username,
                             @"password"   : password
                             };
    
    [self POST:@"token" parameters:params finish:^(id responseObject, NSError *error) {
        if (!error) {
            _currentToken = [[EPToken alloc] initWithDict:responseObject];
        }
        finish_block(responseObject, error);
    }];
    
}

-(void)getCurrentUserProfileWithFinish:(void(^)(id responseObject, NSError *error))finish_block {
    if (!_currentToken) {
        return;
    }
    
    NSString *authorization = [NSString stringWithFormat:@"%@ %@", _currentToken.tokenType, _currentToken.accessToken];

   [_manager.requestSerializer setValue:authorization forHTTPHeaderField:@"Authorization"];
    
    [self GET:@"profile/current" parameters:nil finish:^(id responseObject, NSError *error) {
        NSLog(@"response %@ error %@", responseObject, error);
        if (finish_block) {
            finish_block(responseObject, error);
        }
    }];
}

#pragma mark - HTTP mettods

- (void) POST:(NSString *)URLString parameters:(id)parameters finish:(void (^)(id responseObject, NSError *error))finish_block {
#ifdef  EN_LOGS
    NSLog(@"POST: %@ parameters: %@", URLString, parameters);
#endif
    
    [_manager POST:[API_URL stringByAppendingString:URLString] parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (finish_block) {
            finish_block(responseObject, nil);
        }
        NSLog(@"Response = %@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (finish_block) {
            finish_block(nil, error);
        }
        NSLog(@"Error = %@", error);
    }];
}

- (void) GET:(NSString *)URLString parameters:(id)parameters finish:(void (^)(id responseObject, NSError *error))finish_block {
    
    [_manager GET:[API_URL stringByAppendingString:URLString] parameters:parameters progress:nil
    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (finish_block) {
            finish_block(responseObject, nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (finish_block) {
            finish_block(nil, error);
        }
        NSLog(@"Error = %@", error);
    }];
}

@end
