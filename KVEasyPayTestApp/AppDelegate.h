//
//  AppDelegate.h
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

