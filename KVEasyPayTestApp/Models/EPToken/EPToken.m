//
//  EPToken.m
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "EPToken.h"

@implementation EPToken

- (instancetype)initWithDict:(NSDictionary*)dict {
    self = [super init];
    
    if (self) {
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"Ddd, dd Mmm yyyy, HH:mm:ss"];
        _expiresDate = [formater dateFromString:dict[@".expires"]];
        
        _expires = dict[@".expires"];
        _issued = dict[@".issued"];
        _expiresIn = dict[@"expires_in"];
        _accessToken = dict[@"access_token"];
        _refreshToken = dict[@"refresh_token"];
        _userName = dict[@"user_name"];
        _tokenType = dict[@"token_type"];
    }
    
    return self;
}

@end
