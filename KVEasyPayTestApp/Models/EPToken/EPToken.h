//
//  EPToken.h
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPToken : NSObject

@property (copy, nonatomic) NSString *expires;
@property (copy, nonatomic) NSDate *expiresDate;

@property (copy, nonatomic) NSString *issued;

@property (copy, nonatomic) NSString *accessToken;
@property (copy, nonatomic) NSString *expiresIn;
@property (copy, nonatomic) NSString *refreshToken;
@property (copy, nonatomic) NSString *tokenType;
@property (copy, nonatomic) NSString *userName;

- (instancetype)initWithDict:(NSDictionary*)dict;

@end
