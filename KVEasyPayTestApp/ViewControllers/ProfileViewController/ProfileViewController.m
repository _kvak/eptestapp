//
//  ProfileViewController.m
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//

#import "ProfileViewController.h"
#import "APIClient.h"
#import "MBProgressHUD.h"

@interface ProfileViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIButton *paymentsCheckBox;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLabels];
    
    [self.navigationItem setTitle:@"Profile"];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     [API getCurrentUserProfileWithFinish:^(id responseObject, NSError *error) {
         if (!error) {
             if (![responseObject[@"fName"] isKindOfClass:[NSNull class]] && ![responseObject[@"lName"] isKindOfClass:[NSNull class]]) {
                 _nameLabel.text = [NSString stringWithFormat:@"%@ %@", responseObject[@"fName"], responseObject[@"lName"]];
             } else {
                 _nameLabel.text = @"there is no entered name";
             }
             if (![responseObject[@"email"] isKindOfClass:[NSNull class]]) {
                 _emailLabel.text = responseObject[@"email"];
             } else {
                 _emailLabel.text = @"no email was set";
             }
             _phoneLabel.text = [NSString stringWithFormat:@"+%@", responseObject[@"phone"]];
         } else {
             [self handleError:error];
         }
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleCheckBox:(UIButton*)sender {
    sender.selected = !sender.selected;
}

- (void)setupLabels {
    [_paymentsCheckBox setImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
