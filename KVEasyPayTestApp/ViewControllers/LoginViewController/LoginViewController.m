//
//  LoginViewController.m
//  KVEasyPayTestApp
//
//  Created by Andre Kvashuk on 11/3/16.
//  Copyright © 2016 Andre Kvashuk. All rights reserved.
//
#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "APIClient.h"
#import "MBProgressHUD.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupRegisterButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submit:(id)sender {
    [self.userNameTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    
    if ([self isVerified]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [API loginWithUsername:_userNameTF.text andPassword:_passwordTF.text withFinish:^(BOOL success, NSError *error) {
            if (!error) {
                [self showProfileVC];
            } else {
                [self handleError:error];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
}
- (void)showProfileVC {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle bundleForClass:[self class]]];
    ProfileViewController *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
    
    [self.navigationController pushViewController:profileVC animated:YES];
}

- (BOOL)isVerified {
    BOOL verified;
    
    verified = (_userNameTF.text.length != 0 && _passwordTF.text.length != 0); //very complex verification))))
    
    return verified;
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - View setup

- (void)setupRegisterButton {
    _registerButton.layer.borderWidth = 1;
    _registerButton.layer.borderColor = [UIColor orangeColor].CGColor;
}

@end
